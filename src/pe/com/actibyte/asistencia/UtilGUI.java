/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.actibyte.asistencia;

import java.awt.AlphaComposite;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

/**
 *
 * @author charly
 */
public class UtilGUI {
    public static String RUTA_CACHE="cache/";
    public static void guardarImagen(String nombre, File archivo){
        try {
            InputStream imagen = new FileInputStream(archivo);
            guardarImagen(nombre,imagen);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(UtilGUI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void guardarImagen(String nombre, InputStream contenido){
        OutputStream outputStream=null;
        try {
            File archivo=new File(RUTA_CACHE+nombre);
            if (archivo.delete()) {
                System.out.println("borrado");
            }
            outputStream = new FileOutputStream(archivo);
              int read = 0;
            byte[] bytes = new byte[1024];

            while ((read = contenido.read(bytes)) != -1) {
                    outputStream.write(bytes, 0, read);
            }
            outputStream.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(UtilGUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(UtilGUI.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            if (contenido != null) {
                try {
                    contenido.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (outputStream != null) {
                try {
                    // outputStream.flush();
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        System.out.println("Guardado!");
    }
    
    public static void redimensionar(File imagen, int ancho, int alto){
        try{
 
		BufferedImage originalImage = ImageIO.read(imagen);
		int type = originalImage.getType() == 0? BufferedImage.TYPE_INT_ARGB : originalImage.getType();
 
		BufferedImage resizeImageHintJpg = redimensionarImagen(originalImage, type, ancho,alto);
		ImageIO.write(resizeImageHintJpg, "jpg", imagen); 
 
		/*BufferedImage resizeImageHintPng = redimensionarImagen(originalImage, type, ancho,alto);
		ImageIO.write(resizeImageHintPng, "png", imagen); */
 
	}catch(IOException e){
		System.out.println(e.getMessage());
	}
    }
    
     private static BufferedImage redimensionarImagen(BufferedImage originalImage, int type, int ancho,int alto){
	BufferedImage resizedImage = new BufferedImage(ancho, alto, type);
	Graphics2D g = resizedImage.createGraphics();
        
	g.drawImage(originalImage, 0, 0, ancho, alto, null);
        
	g.dispose();	
	g.setComposite(AlphaComposite.Src);
 
	g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
	RenderingHints.VALUE_INTERPOLATION_BILINEAR);
	g.setRenderingHint(RenderingHints.KEY_RENDERING,
	RenderingHints.VALUE_RENDER_QUALITY);
	g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
	RenderingHints.VALUE_ANTIALIAS_ON);
	return resizedImage;
    }
 
    
    public static ImageIcon leerImagen(String nombre) {
        
        File archivo=new File(RUTA_CACHE+nombre);
        if (archivo.exists()==false) {
            return null;
        }
        
        ImageIcon vistaPrevia=null;
        
        try {
            FileInputStream imagen = new FileInputStream(archivo);
            leerImagen(imagen);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(UtilGUI.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        
        System.out.println("Cargando imagen "+RUTA_CACHE+nombre);
        return vistaPrevia;
    }
    
    public static ImageIcon leerImagen(File archivo){
        ImageIcon vistaPrevia=null;
        try {
            FileInputStream imagen = new FileInputStream(archivo);
            vistaPrevia=leerImagen(imagen);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(UtilGUI.class.getName()).log(Level.SEVERE, null, ex);
        }
        return vistaPrevia;
    }
    
    public static ImageIcon leerImagen(InputStream imagen){
       ImageIcon vistaPrevia=null;
       try {
            //imagen = new FileInputStream(archivo);
            Image imagenEscala = ImageIO.read(imagen).getScaledInstance(100,100, Image.SCALE_SMOOTH);
            vistaPrevia=new ImageIcon(imagenEscala);
            //imagen.reset();
            imagen.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(UtilGUI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(UtilGUI.class.getName()).log(Level.SEVERE, null, ex);
        }
       return vistaPrevia;
    }
}
