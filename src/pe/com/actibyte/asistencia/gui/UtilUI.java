/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.actibyte.asistencia.gui;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import pe.com.actibyte.core.modelo.dao.DAOAsistencia;
import pe.com.actibyte.core.modelo.dao.DAOMarcacion;
import pe.com.actibyte.core.modelo.dao.DAOSuspencion;
import pe.com.actibyte.core.modelo.dao.DAOTurno;
import pe.com.actibyte.core.modelo.vo.Asistencia;
import pe.com.actibyte.core.modelo.vo.Marcacion;
import pe.com.actibyte.core.modelo.vo.ReportePersonal;
import pe.com.actibyte.core.modelo.vo.SuspencionLaboral;
import pe.com.actibyte.core.modelo.vo.TipoHorario;
import pe.com.actibyte.core.modelo.vo.Trabajador;
import pe.com.actibyte.core.modelo.vo.Turno;

/**
 *
 * @author charly
 */
public class UtilUI {
    
    public static String calcularTiempo(Date entrada, Date salida){
        String valor="0";
        long conteo=salida.getTime()-entrada.getTime();
        System.out.println("salida:"+salida.getTime());
        System.out.println("entrada:"+entrada.getTime());
        System.out.println("conteo"+conteo);
        if(conteo>0){
            valor=pe.com.actibyte.util.Util.obtenerConteoHoras(conteo);
        }
        return valor;
    }
    
    public static List<Date> obtenerDiasTranscurridos(Date desde, Date hasta) {
        List<Date> rangoDias=new ArrayList();
        
        Date actual=desde;
        
        while(actual.getTime()<=hasta.getTime()){
            rangoDias.add(actual);
            
            Calendar c=Calendar.getInstance();
            c.setTime(actual);
            c.set(Calendar.HOUR, 0);
            c.set(Calendar.MINUTE, 0);
            c.set(Calendar.SECOND, 0);
            c.set(Calendar.MILLISECOND, 0);
            
            c.add(Calendar.DATE, 1);
            actual=c.getTime();
        }
        
        return rangoDias;
    }

    public static ReportePersonal llenarReporte(ReportePersonal reporte, Date fechaInicio, Date fechaFinal) {
        //ReportePersonal reporte=new ReportePersonal(trabajador);
        
        Trabajador trabajador=reporte.getTrabajador();
        List<Date> diasTranscurridos=UtilUI.obtenerDiasTranscurridos(
                fechaInicio,fechaFinal);
        
        //cargar Turnos;
        DAOTurno daoT=new DAOTurno();
        
        List<Turno> turnos=new ArrayList();
        if(trabajador.getTipoHorario()==TipoHorario.POR_CARGO){
            turnos=daoT.obtenerTurnosPorCargo(trabajador.getCargo());
        }else if(trabajador.getTipoHorario()==TipoHorario.POR_TRABAJADOR){
            turnos=daoT.obtenerTurnosPorTrabajador(trabajador);
        }else if(trabajador.getTipoHorario()==TipoHorario.PROGRAMADO){
            turnos=daoT.obtenerTurnosProgramado(trabajador, fechaInicio,fechaFinal);
        }
        
        
        //cargar marcaciones
        DAOMarcacion daoM=new DAOMarcacion();
        List<Marcacion> marcaciones=daoM.obtenerMarcaciones(trabajador, 
                fechaInicio,fechaFinal);
        
        //cargar asistencias justificadas
        DAOAsistencia daoA=new DAOAsistencia();
        List<Asistencia> asistenciasJustificadas=
                daoA.obtenerAsistenciasJustificadas(trabajador, 
                fechaInicio,fechaFinal);
        
        //cargar suspencionTrabajo
        DAOSuspencion daoS=new DAOSuspencion();
        List<SuspencionLaboral> suspenciones=daoS.obtenerDiasNoLaborables(
                fechaInicio,fechaFinal);
    
        
        reporte.setTurnos(turnos);
        reporte.setMarcaciones(marcaciones);
        reporte.setAsistenciasJustificadas(asistenciasJustificadas);
        reporte.setDiasTranscurridos(diasTranscurridos);
        reporte.setSuspencionesLaborales(suspenciones);
        reporte.procesarControlAsistencia();
        
        return reporte;
    }
}
