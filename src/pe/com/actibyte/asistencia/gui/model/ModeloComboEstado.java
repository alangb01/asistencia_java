/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.actibyte.asistencia.gui.model;

import java.util.List;
import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;
import pe.com.actibyte.core.modelo.vo.Estado;

/**
 *
 * @author charly
 */
public class ModeloComboEstado 
        extends AbstractListModel
        implements ComboBoxModel {
    
    private List<Estado> estados;
    private Estado seleccionado;


    public List<Estado> getEstados() {
      return estados;
    }

    public void setEstados(List<Estado> estados) {
        this.estados = estados;
        this.fireContentsChanged(this, -1,-1);
       
    }
    
    @Override
    public int getSize() {
        int cantidad = 0;

        if (this.estados != null) {
            cantidad = this.estados.size();
        }
        return cantidad;
    }

    @Override
    public Object getElementAt(int index) {
        Object valor = "";
        Estado e;

        if (this.estados != null) {
            e = this.estados.get(index);
            valor = e;
        }

        return valor;
    }

    @Override
    public void setSelectedItem(Object nombreEstado) {
        this.seleccionado = null;

        if (this.estados != null && nombreEstado != null) {
            for (Estado e : estados) {
                if (e==nombreEstado) {
                    this.seleccionado = e;
                    return;
                }
            }
        }
    }

    @Override
    public Object getSelectedItem() {
        Object valor = "";

        if (this.seleccionado != null) {
            valor = this.seleccionado;
        }
        return valor;
    }
    
    public void seleccionarEstado(Estado estado) {
        
        this.seleccionado = null;
        if (this.estados != null && estado != null) {
            for (Estado e : estados) {
                if (e==estado) {
                  this.seleccionado = e;
                  return;
                }
            }
        }
    }
    
}
