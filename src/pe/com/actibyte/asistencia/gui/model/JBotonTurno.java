/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.actibyte.asistencia.gui.model;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import pe.com.actibyte.core.modelo.vo.Turno;

/**
 *
 * @author charly
 */
public class JBotonTurno extends JButton {
    private Turno turno;
    
    
    public Turno getTurno() {
        return turno;
    }

    public void setTurno(Turno turno) {
        this.turno = turno;
    }
    
    public void seleccionar(boolean valor){
        if(valor){
            this.setBackground(Color.GRAY);
        }else{
            this.setBackground(Color.WHITE);
        }
        this.setSelected(valor);
    }
}
