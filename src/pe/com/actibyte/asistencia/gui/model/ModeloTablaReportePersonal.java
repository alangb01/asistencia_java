/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.actibyte.asistencia.gui.model;

import java.util.List;
import javax.swing.table.AbstractTableModel;
import pe.com.actibyte.core.modelo.vo.Cargo;
import pe.com.actibyte.core.modelo.vo.ReportePersonal;

/**
 *
 * @author charly
 */
public class ModeloTablaReportePersonal extends AbstractTableModel{
    private List<ReportePersonal> reportes;

    public List<ReportePersonal> getReportes() {
        return reportes;
    }

    public void setReportes(List<ReportePersonal> reportes) {
        this.reportes = reportes;
        this.fireTableDataChanged();
    }
    
    @Override
    public int getRowCount() {
        int cantidad = 0;
        if (this.reportes != null) {
          cantidad = this.reportes.size();
        }
        return cantidad;
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        ReportePersonal r;
        Object valor = "";

        if (this.reportes != null) {
            r = this.reportes.get(rowIndex);
            switch (columnIndex) {
                case 0:
                    valor = r.getTrabajador().toString();
                    break;
                case 1:
                    valor = r.getTrabajador().getCorreo();
                    break;
               
                case 2:
                    valor = r.getEstadoPdfGeneracion();
                    break;
                case 3:
                    valor = r.getEstadoPdfEnvio();
                    break;
            }
        }

        return valor;
    }
    
    @Override
    public String getColumnName(int column) {
        String titulo = "";

        switch (column) {
            case 0:
                titulo = "Trabajador";
                break;
            case 1:
                titulo = "Correo";
                break;
            case 2:
                titulo = "Archivo";
                break;
            case 3:
                titulo = "Enviado";
                break;
        }
        return titulo;
    }
}

