/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.actibyte.asistencia.gui.model;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import pe.com.actibyte.core.modelo.vo.Marcacion;
import pe.com.actibyte.util.Util;

/**
 *
 * @author charly
 */
public class ModeloTablaMarcacion extends AbstractTableModel {
    private List<Marcacion> marcaciones=new ArrayList();

    public List<Marcacion> getMarcaciones() {
        return marcaciones;
    }

    public void setMarcaciones(List<Marcacion> marcaciones) {
        this.marcaciones = marcaciones;
        this.fireTableDataChanged();
    }
    
    
    
    @Override
    public int getRowCount() {
        int cantidad = 0;
        if (this.marcaciones != null) {
          cantidad = this.marcaciones.size();
        }
        return cantidad;
    }

    @Override
    public int getColumnCount() {
        return 7;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Marcacion m;
        Object valor = "";

        if (this.marcaciones != null) {
            m = this.marcaciones.get(rowIndex);
            SimpleDateFormat sdf=null;
            switch (columnIndex) {
                case 0:
                    Calendar c=Calendar.getInstance();
                    c.setTime(m.getFechaHoraMarcado());
                    valor = Util.obtenerDia(c.get(Calendar.DAY_OF_WEEK));
                    break;
                case 1:
                    sdf = new SimpleDateFormat("YYYY-MM-dd");
                    valor =  sdf.format(m.getFechaHoraMarcado());
                    break;
                case 2:
                    sdf = new SimpleDateFormat("HH:mm:ss");
                    valor =  sdf.format(m.getFechaHoraMarcado());
                    break;
                case 3:
                    valor = m.getTipoMarcacion();
                    break;
                case 4:
                    if(m.getTipoJustificacion()!=null){
                        valor = m.getTipoJustificacion();
                    }else{
                        valor ="no definido";
                    }
                    break;
                case 5:
                    if(m.getObservacion()!=null){
                        valor = m.getObservacion();
                    }else{
                        valor ="---";
                    }
                    break;
                case 6:
                    if (m.getEstado()!=null) {
                        valor = m.getEstado();
                    }
                    break;
                default:
                    valor="no definido";
                    break;
            }
        }

        return valor;
        
    }
    
    @Override
    public String getColumnName(int column) {
        String titulo = "";

        switch (column) {
            case 0:
                titulo = "Dia";
                break;
            case 1:
                titulo = "Fecha Marcada";
                break;
            case 2:
                titulo = "Hora Marcada";
                break;
            case 3:
                titulo = "Marcacion";
                break;
            case 4:
                titulo = "Tipo";
                break;
            case 5:
                titulo= "Observacion";
                break;
            case 6:
                titulo= "Estado";
                break;
            default :
                titulo="no definido";
                break;  
        }
        return titulo;
    }
    
}
