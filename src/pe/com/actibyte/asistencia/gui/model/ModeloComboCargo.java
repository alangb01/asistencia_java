package pe.com.actibyte.asistencia.gui.model;

import java.util.List;
import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;
import pe.com.actibyte.core.modelo.vo.Cargo;

public class ModeloComboCargo
        extends AbstractListModel
        implements ComboBoxModel {

    private List<Cargo> cargos;
    private Cargo seleccionado;
    public List<Cargo> getCargos() {
      return cargos;
    }
    
    public void setCargos(List<Cargo> cargos) {
        this.cargos = cargos;
        this.fireContentsChanged(this, -1,-1);
       
    }

    @Override
    public int getSize() {
        int cantidad = 0;

        if (this.cargos != null) {
            cantidad = this.cargos.size();
        }
        return cantidad;
    }

    @Override
    public Object getElementAt(int index) {
        Object valor = "";
        Cargo c;

        if (this.cargos != null) {
            c = this.cargos.get(index);
            valor = c.getNombre();
        }

        return valor;
    }

    @Override
    public void setSelectedItem(Object nombreCargo) {
        this.seleccionado = null;

        if (this.cargos != null && nombreCargo != null) {
            for (Cargo c : cargos) {
            if (c.getNombre().equals(nombreCargo.toString()) 
                  == true) {
                this.seleccionado = c;
                return;
                }
            }
        }
    }

    public void seleccionarCargo(Cargo cargo) {
        this.seleccionado = null;
        if (this.cargos != null && cargo != null) {
            for (Cargo c : cargos) {
                if (c.getIdCargo() == cargo.getIdCargo() ) {
                  this.seleccionado = c;
                  return;
                }
            }
        }
    }
  
    @Override
    public Object getSelectedItem() {
        Object valor = "";

        if (this.seleccionado != null) {
            valor = this.seleccionado.getNombre();
        }
        return valor;
    }
  
}
