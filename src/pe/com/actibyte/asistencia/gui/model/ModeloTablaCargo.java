/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.actibyte.asistencia.gui.model;

import java.util.List;
import javax.swing.table.AbstractTableModel;
import pe.com.actibyte.core.modelo.vo.Cargo;

/**
 *
 * @author charly
 */
public class ModeloTablaCargo extends AbstractTableModel{
    private List<Cargo> cargos;

    public List<Cargo> getCargos() {
        return cargos;
    }

    public void setCargos(List<Cargo> cargos) {
        this.cargos = cargos;
       
        this.fireTableDataChanged();
    }
    
    @Override
    public int getRowCount() {
        int cantidad = 0;
        if (this.cargos != null) {
          cantidad = this.cargos.size();
        }
        return cantidad;
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Cargo c;
        Object valor = "";

        if (this.cargos != null) {
            c = this.cargos.get(rowIndex);
            switch (columnIndex) {
                case 0:
                    valor = c.getIdCargo();
                    break;
                case 1:
                    valor = c.getNombre();
                    break;
               
                case 2:
                    if (c.getEstado()!=null) {
                        valor = c.getEstado();
                    } 
                    break;
            }
        }

        return valor;
    }
    
    @Override
    public String getColumnName(int column) {
        String titulo = "";

        switch (column) {
            case 0:
                titulo = "Nro";
                break;
            case 1:
                titulo = "Nombre";
                break;
            case 2:
                titulo = "Estado";
                break;
        }
        return titulo;
    }
}

