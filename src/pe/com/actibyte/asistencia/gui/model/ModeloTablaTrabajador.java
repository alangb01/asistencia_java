package pe.com.actibyte.asistencia.gui.model;

import java.util.List;
import javax.swing.JButton;
import javax.swing.table.AbstractTableModel;
import pe.com.actibyte.core.modelo.vo.Estado;
import pe.com.actibyte.core.modelo.vo.Trabajador;

public class ModeloTablaTrabajador
        extends AbstractTableModel {

  private List<Trabajador> trabajadores;

  public List<Trabajador> getTrabajadores() {
    return trabajadores;
  }

  public void setTrabajadores(List<Trabajador> trabajadores) {
    this.trabajadores = trabajadores;
    this.fireTableDataChanged();
  }

  @Override
  public int getRowCount() {
    int cantidad = 0;

    if (this.trabajadores != null) {
      cantidad = this.trabajadores.size();
    }
    return cantidad;
  }

  @Override
  public int getColumnCount() {
    return 7;
  }

  @Override
  public Object getValueAt(int rowIndex, int columnIndex) {
    Trabajador t;
    Object valor = "";

    if (this.trabajadores != null) {
        t = this.trabajadores.get(rowIndex);
        switch (columnIndex) {
            case 0:
                valor = t.getIdTrabajador();
                break;
            case 1:
                if(t.getCargo()!=null){
                    valor=t.getCargo().getNombre();
                }
                break;
            case 2:
                valor = t.getNombres();
                break;
            case 3:
                valor = t.getApellidos();
                break;
            case 4:
                valor = t.getCorreo();
                 break;
            case 5:
                valor = t.getTelefono();
                break;
            case 6:
                if (t.getEstado()!=null) {
                   valor = t.getEstado();
                } 
                break;
        }
    }

    return valor;
  }

  @Override
  public String getColumnName(int column) {
    String titulo = "";

    switch (column) {
        case 0:
            titulo = "Nro";
            break;
        case 1:
            titulo = "Cargo";
            break;
        case 2:
            titulo = "Nombre";
            break;
        case 3:
            titulo = "Apellidos";
            break;
        case 4:
            titulo = "Correo";
            break;
        case 5:
            titulo = "Telefono";
            break;
        case 6:
            titulo = "Estado";
            break;
        case 7:
            titulo = "Boton";
            
            break;
    }
    return titulo;
  }
}
