/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.actibyte.asistencia.gui.model;

import java.util.List;
import javax.swing.table.AbstractTableModel;
import pe.com.actibyte.core.modelo.vo.Trabajador;
import pe.com.actibyte.core.modelo.vo.Usuario;

/**
 *
 * @author charly
 */
public class ModeloTablaUsuario extends AbstractTableModel{
    private List<Usuario> usuarios;

    public List<Usuario> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<Usuario> usuarios) {
        this.usuarios = usuarios;
        this.fireTableDataChanged();
    }
    
    @Override
    public int getRowCount() {
        int cantidad = 0;

        if (this.usuarios != null) {
          cantidad = this.usuarios.size();
        }
        return cantidad;
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Usuario u;
        Object valor = "";

        if (this.usuarios != null) {
            u = this.usuarios.get(rowIndex);
            switch (columnIndex) {
                case 0:
                    valor = u.getIdUsuario();
                    break;
                case 1:
                    valor = u.getNombreUsuario();
                    break;
                case 2:
                    if(u.getTrabajador()!=null){
                        valor = u.getTrabajador().toString();
                    }else{
                       valor="no asociado";
                    }
                    
                    break;
                case 3:
                    valor = u.getEstado();
                    break;
            }
        }

        return valor;
    }
    @Override
    public String getColumnName(int column) {
      String titulo = "";

        switch (column) {
            case 0:
                titulo = "Id";
                break;
            case 1:
                titulo = "Usuario";
                break;
            case 2:
                titulo = "Trabajador";
                break;
            case 3:
                titulo = "Estado";
                break;
        }
         return titulo;
    }
}
