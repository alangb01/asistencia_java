package pe.com.actibyte.asistencia.gui.model;

import java.util.List;
import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;
import pe.com.actibyte.core.modelo.vo.TipoHorario;

public class ModeloComboTipoHorario
        extends AbstractListModel
        implements ComboBoxModel {

    private List<TipoHorario> tiposHorario;
    private TipoHorario seleccionado;
    public List<TipoHorario> getTiposHorario() {
      return tiposHorario;
    }
    
    public void setTiposHorario(List<TipoHorario> tiposHorario) {
        this.tiposHorario = tiposHorario;
        this.fireContentsChanged(this, -1,-1);
       
    }

    @Override
    public int getSize() {
        int cantidad = 0;

        if (this.tiposHorario != null) {
            cantidad = this.tiposHorario.size();
        }
        return cantidad;
    }

    @Override
    public Object getElementAt(int index) {
        Object valor = "";
        TipoHorario t;

        if (this.tiposHorario != null) {
            t = this.tiposHorario.get(index);
            valor = t.getNombre();
        }

        return valor;
    }

    @Override
    public void setSelectedItem(Object nombre) {
        this.seleccionado = null;

        if (this.tiposHorario != null && nombre != null) {
            for (TipoHorario t : tiposHorario) {
                if (t.getNombre().equals(nombre)) {
                this.seleccionado = t;
                return;
                }
            }
        }
    }

    public void seleccionarTipoHorario(TipoHorario tipoHorario) {
        this.seleccionado = null;
        if (this.tiposHorario != null && tipoHorario != null) {
            for (TipoHorario t : tiposHorario) {
                if (t == tipoHorario ) {
                  this.seleccionado = t;
                  return;
                }
            }
        }
    }
  
    @Override
    public Object getSelectedItem() {
        Object valor = "";
        
        if (this.seleccionado != null) {
            valor = this.seleccionado.getNombre();
        }
        return valor;
    }
  
}
