/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.actibyte.asistencia.gui.model;

import java.text.SimpleDateFormat;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import pe.com.actibyte.core.modelo.vo.SuspencionLaboral;

/**
 *
 * @author charly
 */
public class ModeloTablaDiaNoLaborable extends AbstractTableModel{
    private List<SuspencionLaboral> diasNoLaborables;

    public List<SuspencionLaboral> getDiasNoLaborables() {
        return diasNoLaborables;
    }

    public void setDiasNoLaborables(List<SuspencionLaboral> diasNoLaborables) {
        this.diasNoLaborables = diasNoLaborables;
        this.fireTableDataChanged();
    }
    
    
    @Override
    public int getRowCount() {
        int cantidad = 0;
        if (this.diasNoLaborables != null) {
          cantidad = this.diasNoLaborables.size();
        }
        return cantidad;
    }

    @Override
    public int getColumnCount() {
         return 4;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        SuspencionLaboral f;
        Object valor = "";

        if (this.diasNoLaborables != null) {
            f = this.diasNoLaborables.get(rowIndex);
            switch (columnIndex) {
                case 0:
                    valor = f.getId();
                    break;
                case 1:
                    SimpleDateFormat sdf=new SimpleDateFormat("YYYY-MM-dd");
                    valor = sdf.format(f.getFecha());
                    break;
                case 2:
                    valor = f.getDescripcion();
                    break;
                case 3:
                    if(f.isRepeticionAnual()){
                        valor="Si";
                    }else{
                        valor="No";
                    }
                    break;
                default:
                    valor = "no definido";
                    break;
            }
        }

        return valor;
    }
    
    @Override
    public String getColumnName(int column) {
        String titulo = "";

        switch (column) {
            case 0:
                titulo = "Id";
                break;
            case 1:
                titulo = "Fecha";
                break;
            case 2:
                titulo = "Descripción";
                break;
            case 3:
                titulo = "Repeticion anual";
                break;
            default:
                titulo = "no definido";
                break;
        }
        return titulo;
    }
}
