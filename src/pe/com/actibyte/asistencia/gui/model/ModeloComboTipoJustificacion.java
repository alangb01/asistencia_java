/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.actibyte.asistencia.gui.model;

import java.util.List;
import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;
import pe.com.actibyte.core.modelo.vo.TipoJustificacion;

/**
 *
 * @author charly
 */
public class ModeloComboTipoJustificacion  extends AbstractListModel
        implements ComboBoxModel {
    private List<TipoJustificacion> justificaciones;
    private TipoJustificacion seleccionado;

    public List<TipoJustificacion> getJustificaciones() {
        return justificaciones;
    }

    public void setJustificaciones(List<TipoJustificacion> justificaciones) {
        this.justificaciones = justificaciones;
    }
    @Override
    public int getSize() {
        int cantidad = 0;

        if (this.justificaciones != null) {
            cantidad = this.justificaciones.size();
        }
        return cantidad;
    }

    @Override
    public Object getElementAt(int index) {
        Object valor = "";
        TipoJustificacion c;

        if (this.justificaciones != null) {
            c = this.justificaciones.get(index);
            valor = c;
        }

        return valor;
    }

    @Override
    public void setSelectedItem(Object nombreEstado) {
        this.seleccionado = null;

        if (this.justificaciones != null && nombreEstado != null) {
            for (TipoJustificacion j : justificaciones) {
                if (j==nombreEstado) {
                    this.seleccionado = j;
                    return;
                }
            }
        }
    }

    @Override
    public Object getSelectedItem() {
        Object valor = "";

        if (this.seleccionado != null) {
            valor = this.seleccionado;
        }
        return valor;
    }
    public void seleccionarEstado(TipoJustificacion justificacion) {
        
        this.seleccionado = null;
        if (this.justificaciones != null && justificacion != null) {
            for (TipoJustificacion j : justificaciones) {
                if (j==justificacion) {
                  this.seleccionado = j;
                  return;
                }
            }
        }
    }
}
