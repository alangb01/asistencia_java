/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.actibyte.asistencia.gui.model;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import pe.com.actibyte.core.modelo.vo.Dispositivo;

/**
 *
 * @author charly
 */
public class ModeloTablaDispositivo extends AbstractTableModel {
    private List<Dispositivo> dispositivos=new ArrayList();

    public List<Dispositivo> getDispositivos() {
        return dispositivos;
    }

    public void setDispositivos(List<Dispositivo> dispositivos) {
        this.dispositivos = dispositivos;
        this.fireTableDataChanged();
    }
    
    @Override
    public int getRowCount() {
       int cantidad = 0;
        if (this.dispositivos != null) {
          cantidad = this.dispositivos.size();
        }
        return cantidad;
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Dispositivo d;
        Object valor = "";

        if (this.dispositivos != null) {
            d = this.dispositivos.get(rowIndex);
            switch (columnIndex) {
                case 0:
                    valor = d.getIdDispositivo();
                    break;
                case 1:
                    valor="No asociado";
                    if(d.getTrabajador()!=null){
                        valor=d.getTrabajador().toString();
                    }
                    break;
                case 2:
                    valor = d.getCodigo();
                    break;
                case 3:
                    if (d.getEstado()!=null) {
                        valor = d.getEstado();
                    } 
                    break;
            }
        }

        return valor;
    }
    @Override
    public String getColumnName(int column) {
        String titulo = "";

        switch (column) {
            case 0:
                titulo = "Nro";
                break;
            case 1:
                titulo = "Asociado a";
                break;
            case 2:
                titulo = "Codigo";
                break;
            case 3:
                titulo = "Estado";
                break;
        }
        return titulo;
    }
}
