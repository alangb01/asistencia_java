/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.actibyte.asistencia.gui.model;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import pe.com.actibyte.core.modelo.vo.Turno;
import pe.com.actibyte.util.Util;

/**
 *
 * @author charly
 */
public class ModeloHorario extends JPanel implements ActionListener {
    private List<Turno> turnos;
    private JPanel contenedor_turnos;
    
    private int ancho_turno;
    private double escala_vertical;
    private long hora_maxima;
    private long hora_minima;
    
    private int dia_maximo=6;
    private int dia_minimo=0;
    
    private int primer_dia_semana=Calendar.MONDAY;
    private int ultimo_dia_semana=Calendar.SUNDAY;
    
    //LABEL HORA
    private int ancho_celda=100;
    private int alto_celda=15;

    public List<Turno> getTurnos() {
        return turnos;
    }

    public void setTurnos(List<Turno> turnos) {
        if(this.turnos==null && this.turnos.size()==0){
            System.out.println("anulando");
            return;
        }
        
        this.turnos = turnos;
        this.mostrar();
    }
    
    /**
     *
     */
    public ModeloHorario() {
        this.setBackground(Color.GRAY);
        this.setLayout(null);
        this.turnos=new ArrayList();
        
        
        setBounds(10, 40,600, 15*25);
        mostrar();
    }

    
    
    
    public void mostrar(){
        this.removeAll();
        
        this.configurarParametros();
        
        List<JLabel> listaEtiquetas=this.crearEtiquetas();
        List<JButton> listaTurnos=this.crearEtiquetasTurno();
        
        //ETIQUETAS
        JPanel contenedor_label=new JPanel();
        contenedor_label.setBounds(0, 0, this.getWidth(), this.getHeight());
        contenedor_label.setBorder(javax.swing.BorderFactory.
                    createLineBorder(new java.awt.Color(0, 0, 0)));
        contenedor_label.setLayout(null);
        
        Iterator<JLabel> it=listaEtiquetas.iterator();
        while(it.hasNext()){
            JLabel jp=it.next();
            contenedor_label.add(jp);
        }
        contenedor_label.updateUI();
        add(contenedor_label);
        
        // TURNOS
        contenedor_turnos=new JPanel();
        contenedor_turnos.setName("contenedor_turnos");
        contenedor_turnos.setBounds(this.ancho_celda, this.alto_celda, 
                this.getWidth()-this.ancho_celda , 
                this.getHeight()-this.alto_celda);
        contenedor_turnos.setBorder(javax.swing.BorderFactory.
                    createLineBorder(new java.awt.Color(0, 0, 0)));
        
        contenedor_turnos.setLayout(null);
        contenedor_turnos.setBackground(Color.GRAY);
        
        Iterator<JButton> it_turno=listaTurnos.iterator();
        while(it_turno.hasNext()){
            JButton jp=it_turno.next();
            System.out.println("TURNO: "+jp.getX()+", "+jp.getY()+", "+jp.getWidth()+", "+jp.getHeight());
            contenedor_turnos.add(jp);
        }
        
        contenedor_turnos.updateUI();
        
        contenedor_label.add(contenedor_turnos);
        
        this.updateUI();
    }
    
    public List<JLabel> crearEtiquetas(){
        
        List<JLabel> lista=new ArrayList();
        
        JLabel hora=new JLabel();
        hora.setBounds(0, 0, this.ancho_celda, this.alto_celda);
        hora.setText("Hora");
        hora.setHorizontalAlignment(SwingConstants.CENTER);
        hora.setVerticalAlignment(SwingConstants.CENTER);
        hora.setBorder(javax.swing.BorderFactory.
                    createLineBorder(new java.awt.Color(0, 0, 0)));
        lista.add(hora);
        
        
       
        int posicion=0;
        
        Calendar c=Calendar.getInstance();
        c.set(Calendar.DAY_OF_WEEK, primer_dia_semana);
        
        int dia_actual=c.get(Calendar.DAY_OF_WEEK);
        while(dia_actual!=Calendar.SUNDAY){
            
            dia_actual=c.get(Calendar.DAY_OF_WEEK);
            
            int x=posicion*this.ancho_turno+this.ancho_celda;
            int y=0;
            
            JLabel dia=new JLabel();
            dia.setBounds(x, y, this.ancho_turno, this.alto_celda);
            dia.setText(Util.obtenerDia(dia_actual));
            dia.setHorizontalAlignment(SwingConstants.CENTER);
            dia.setVerticalAlignment(SwingConstants.CENTER);
            dia.setBorder(javax.swing.BorderFactory.
                    createLineBorder(new java.awt.Color(0, 0, 0)));
            lista.add(dia);
            
            c.add(Calendar.DATE, 1);
            posicion++;
        }
        
        int hora_actual=0;
        while(hora_actual<24){
            
            String texto=hora_actual+":00-"+(hora_actual+1)+":00";
            int x=0;
            int y=(int)((hora_actual+1)*this.alto_celda);
           
            int ancho=this.ancho_celda;
            int alto=this.alto_celda;
            
            JLabel dia=new JLabel();
            dia.setBounds(x, y, ancho, alto);
            dia.setText(texto);
            dia.setHorizontalAlignment(SwingConstants.CENTER);
            dia.setVerticalAlignment(SwingConstants.CENTER);
            dia.setBorder(javax.swing.BorderFactory.
                    createLineBorder(new java.awt.Color(0, 0, 0)));
            lista.add(dia);
            
            hora_actual++;
        }
        
        return lista;
    }
    

    private List<JButton> crearEtiquetasTurno() {
        List<JButton> listaTurno=new ArrayList();
        Iterator <Turno> it=this.turnos.iterator();
        while(it.hasNext()){
            Turno turno=it.next();
            
            
            int ancho=ancho_turno;
            int alto=(int)(turno.obtenerTiempo()*escala_vertical);
           
            Calendar c=Calendar.getInstance();
            c.set(Calendar.DAY_OF_WEEK, primer_dia_semana);
        
            int posicion=0;
            posicion=turno.getDia()-primer_dia_semana;
            if(posicion<0){
                posicion+=7;
            }
            
            System.out.println("posicion:"+posicion+", primer:"+primer_dia_semana+", ultimo"+ultimo_dia_semana);
            int x=(posicion)*ancho_turno;
            int y=(int)((turno.getHoraEntrada().getTime()-5*60*60*1000)*escala_vertical);
            
            System.out.println("tiempo "+turno.obtenerTiempo());
            System.out.println("inicio "+turno.getHoraEntrada().getTime());
            SimpleDateFormat sdf=new SimpleDateFormat("HH:mm");
            
            String texto=sdf.format(turno.getHoraEntrada())+"-"
                            +sdf.format(turno.getHoraSalida());
                            
            Color color=Color.WHITE;
            String tooltip=Util.obtenerConteoHoras(turno.obtenerTiempo());
            
            if(turno.obtenerTiempo()>=8*60*60*1000){
                color=Color.red;
                tooltip+="Excedido 8 horas continuas de trabajo.";
            }
            
            JBotonTurno bloqueTurno=new JBotonTurno();
            bloqueTurno.setBounds(x, y, ancho, alto);
            bloqueTurno.setText(texto);
            bloqueTurno.setToolTipText(tooltip);
            bloqueTurno.setBackground(color);
            bloqueTurno.setHorizontalAlignment(SwingConstants.CENTER);
            bloqueTurno.setVerticalAlignment(SwingConstants.CENTER);
            bloqueTurno.setBorder(BorderFactory.createLineBorder(Color.black));
            bloqueTurno.setContentAreaFilled(false);
            bloqueTurno.setOpaque(true);
            bloqueTurno.setTurno(turno);
            bloqueTurno.addActionListener(this);
            listaTurno.add(bloqueTurno);
        }
        return listaTurno;
    }

    private void configurarParametros() {
        this.hora_maxima=1000*60*60*24; 
        this.hora_minima=0;
        
        this.dia_maximo=6;  
        this.dia_minimo=0;  
        
        double rango=(this.hora_maxima-this.hora_minima);
        this.escala_vertical=(this.getHeight()-this.alto_celda)/rango;
        
        this.ancho_turno=(int)((this.getWidth()-this.ancho_celda)/(this.dia_maximo-this.dia_minimo+1));
    }

    

    private int ajustePosicion(int dia) {
       return 0; 
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        JBotonTurno boton=((JBotonTurno) ae.getSource());
        boton.seleccionar(!boton.isSelected());
        
    }
    
    public Turno getTurnoSeleccionado(){
        Turno turno=null;
        List<Turno> lista=this.getTurnosSeleccionados();
        if(lista.size()==1){
            turno=lista.get(0);
        }
        return turno;
    }
    
    public List<Turno> getTurnosSeleccionados(){
        List<Turno> lista=new ArrayList();
        Component[] componentes =contenedor_turnos.getComponents(); 
        for(int i=0; i<componentes.length;i++) { 
            if(((JBotonTurno)componentes[i]).isSelected()){ 
               lista.add(((JBotonTurno)componentes[i]).getTurno());
            } 
        }
        System.out.println("tamaño"+lista.size());
        return lista;
    }
    
    
}
