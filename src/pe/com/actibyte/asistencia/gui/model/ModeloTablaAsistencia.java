/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.actibyte.asistencia.gui.model;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import pe.com.actibyte.core.modelo.vo.Asistencia;
import pe.com.actibyte.util.Util;

/**
 *
 * @author charly
 */
public class ModeloTablaAsistencia extends AbstractTableModel {
    private List<Asistencia> asistencias=new ArrayList();

    public List<Asistencia> getAsistencias() {
        return asistencias;
    }

    public void setAsistencias(List<Asistencia> asistencias) {
        this.asistencias = asistencias;
        this.fireTableDataChanged();
    }
    
    
    
    @Override
    public int getRowCount() {
        int cantidad = 0;
        if (this.asistencias != null) {
          cantidad = this.asistencias.size();
        }
        return cantidad;
    }

    @Override
    public int getColumnCount() {
        return 7;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Asistencia a;
        Object valor = "";

        if (this.asistencias != null) {
            a = this.asistencias.get(rowIndex);
            SimpleDateFormat sdf=null;
            switch (columnIndex) {
                case 0:
                    if(a.getFechaAsistida()!=null){
                        Calendar c=Calendar.getInstance();
                        c.setTime(a.getFechaAsistida());
                        valor = Util.obtenerDia(c.get(Calendar.DAY_OF_WEEK));
                    }else{
                        valor="error";
                    }
                    break;
                case 1:
                    if(a.getFechaAsistida()!=null){
                        sdf = new SimpleDateFormat("YYYY-MM-dd");
                        valor =  sdf.format(a.getFechaAsistida());
                    }else{
                        valor="error";
                    }
                    break;
                case 2:
                    if(a.getEntrada()!=null){
                       sdf = new SimpleDateFormat("HH:mm:ss");
                        valor =  sdf.format(a.getEntrada().getFechaHoraMarcado()); 
                    }else{
                        valor = "---";
                    }
                    
                    break;
                case 3:
                    if(a.getSalida()!=null){
                       sdf = new SimpleDateFormat("HH:mm:ss");
                        valor =  sdf.format(a.getSalida().getFechaHoraMarcado()); 
                    }else{
                        valor = "---";
                    }
                    break;
                case 4:
                    valor =  Util.obtenerConteoHoras(a.getTiempoAcumulado());
                    break;
                case 5:
                    if(a.getTipoJustificacion()!=null){
                        valor = a.getTipoJustificacion();
                    }else{
                        valor ="no definido";
                    }
                    break;
                case 6:
                    if(a.getObservacion()!=null){
                        valor = a.getObservacion();
                    }else{
                        valor ="---";
                    }
                    break;
                default:
                    valor="no definido";
                    break;
            }
        }

        return valor;
        
    }
    
    @Override
    public String getColumnName(int column) {
        String titulo = "";

        switch (column) {
            case 0:
                titulo = "Dia";
                break;
            case 1:
                titulo = "Fecha";
                break;
            case 2:
                titulo = "Hora Entrada";
                break;
            case 3:
                titulo = "Hora Salida";
                break;
            case 4:
                titulo = "Tiempo";
                break;
            case 5:
                titulo = "Tipo";
                break;
            case 6:
                titulo= "Observacion";
                break;
            default :
                titulo="no definido";
                break;  
        }
        return titulo;
    }
    
}
