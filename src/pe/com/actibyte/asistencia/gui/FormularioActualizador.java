/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.actibyte.asistencia.gui;

/**
 *
 * @author charly
 */
public interface FormularioActualizador {
    public void setListado(ListadoActualizable ls);
    public void actualizarListado();
    public boolean verificarDatos();
}
