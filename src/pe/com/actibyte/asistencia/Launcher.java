/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pe.com.actibyte.asistencia;

import javax.swing.UIManager;
import pe.com.actibyte.asistencia.gui.InterfazAsistencia;
import pe.com.actibyte.core.acceso.BaseDatos;
import pe.com.actibyte.core.acceso.ServicioBD;

/**
 *
 * @author charly
 */
public class Launcher {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        try { 
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel"); 
        } catch (Exception ex) { 
            ex.printStackTrace(); 
        }
        
        BaseDatos.setConfig(ServicioBD.SERVIDOR);
        InterfazAsistencia app=new InterfazAsistencia();
    }
    
    
}
